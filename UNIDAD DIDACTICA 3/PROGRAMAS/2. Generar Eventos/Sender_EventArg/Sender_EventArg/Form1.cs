﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sender_EventArg
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        //cickar en el boton ya sea con el ratón o con el enter
        private void button1_Click(object sender, EventArgs e)
        {
            Button btn = sender;
            MessageBox.Show(btn.Text);
        }


        //ejemplo de otro evento que desencadena en lugar de click eligo MouseClick
        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show(Convert.ToString(e.Location));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click con con cualquier dispositivo");
        }

        private void button3_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Click ");
        }
    }
}
