﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double grados;
        private double datoCajaTexto;

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            TextBox objTextBox = (TextBox)sender;
            try
            {
                datoCajaTexto = Convert.ToDouble(objTextBox.Text);
            }
            catch (Exception)
            {

                e.Cancel = true;
                objTextBox.SelectAll();
                ProveedorError1.SetError(objTextBox, "Tiene que ser numérico");
            }
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox textbox1_c = (TextBox)sender;

        }
    


    private void button1_Click(object sender, EventArgs e)
        {

            grados = Convert.ToSingle(textBox1.Text) * 9 / 5 + 32;
            textBox2.Text = grados.ToString();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox textbox1_k = (TextBox)sender;
            //grados = Convert.ToSingle(textBox1.Text) * 9 / 5 + 32;
            textBox2.Text = string.Format("{0}", grados);
        }


    }
}
