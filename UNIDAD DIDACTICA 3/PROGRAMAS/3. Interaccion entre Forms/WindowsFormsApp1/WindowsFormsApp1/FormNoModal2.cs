﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormNoModal2 : Form
    {
        private Form padre;
        public FormNoModal2(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
        }

        private void FormNoModal2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ((Form1)padre).MuestraLabel(); //Accedo al metodo padre MuestraLabel
        }
    }
}
