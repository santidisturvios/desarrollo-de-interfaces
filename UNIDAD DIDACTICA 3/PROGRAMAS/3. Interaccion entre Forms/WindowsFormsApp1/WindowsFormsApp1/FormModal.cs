﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormModal : Form
    {
        public int Peso; //{ get; set; }
        public FormModal()
        {
            InitializeComponent();
        }


        private void FormModal_Load(object sender, EventArgs e)
        {
            textBox1.Text = Peso.ToString(); //ojo convertir a string, cojo la varialbe peso del primer Form

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Peso = Convert.ToInt16(textBox1.Text); //modifico la variable Peso
        }
    }
}
