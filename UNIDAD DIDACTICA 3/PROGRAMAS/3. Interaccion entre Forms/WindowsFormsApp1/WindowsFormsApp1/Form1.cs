﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private int peso = 20;
        public Form1()
        {
            InitializeComponent();
        }

        //Modal
        private void button1_Click(object sender, EventArgs e)
        {
            FormModal frmMod = new FormModal(); //Creo el objeto de la ventana modal

            frmMod.Peso = 20; //cargamos el dato en el objeto

            //frmMod.ShowDialog(); //Muestro el objeto de la ventana modal

            if (frmMod.ShowDialog() == DialogResult.OK) //modifico en propiedades del elemento
            {
                peso = frmMod.Peso;
                label1.Text = "El peso es " + peso;
            }
            else { }

        }

        //No Modal
        private void button2_Click(object sender, EventArgs e)
        {
            FormNoModal frmNoMod = new FormNoModal(this);
            frmNoMod.Show(); //solo necesita un show ya que es no modal
        }



        //No Modal 2
        private void button3_Click(object sender, EventArgs e)
        {
            FormNoModal2 frmNoMod2 = new FormNoModal2(this); //creo el objeto y le paso los datos de la clase donde estoy
            frmNoMod2.Show(); //solo necesita un show ya que es no modal
        }


        public void MuestraLabel()
        {
            label2.Visible = true; //ahora necesito desde la ventana no modal llamar a la funcion

        }

    }
}
