﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _6.WindowsFormsMDI
{
    public partial class Form1 : Form
    {
        private int index = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form FrmChild = new Form();

            index++;
            
            FrmChild.MdiParent = this;
            FrmChild.Text = "Ventana hija número" + index;
            FrmChild.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ActiveMdiChild != null) {
                ActiveMdiChild.Close();
            
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void newFormPersonalizedToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FormHijo FrmChild = new FormHijo();

            index++;

            FrmChild.MdiParent = this;
            FrmChild.Text = "Ventana hija número" + index;
            FrmChild.Show();

        }
    }
}
