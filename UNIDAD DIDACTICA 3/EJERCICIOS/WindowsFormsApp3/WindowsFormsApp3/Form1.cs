﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Properties;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {

        
        public static string[] usuarios = {"Juan", "Pepe", "Blas" };
        public static string[] passwords = { "123", "456", "789" };
        public int intentos = 3; 
        public Form1()
        {
            InitializeComponent();
            label1.Text = "Numero de intentos" +Convert.ToString(intentos);
        }

        private void button1_Click(object sender, EventArgs e)
        {
             if (Metodo(textBox1.Text, textBox2.Text) == true)
                {
                    MessageBox.Show("Usuario autenticado");
                }

                if (Metodo(textBox1.Text, textBox2.Text) == false)
                {
                    MessageBox.Show("Constraseña Incorrecta");
                }

                intentos--;

                label1.Text = "Numero de intentos" + Convert.ToString(intentos);

            if (intentos == 0) {
                button1.Enabled = false;
            }

            


        }

        public static bool Metodo(string nombre, string passw) {

            bool resultado = false;

            for (int i=0; i<2; i++){

                if (nombre==usuarios[i] && passw==passwords[i]){

                    resultado = true;
                    break;
                    
                }

                else{

                    resultado = false;
                }

            
               
            }

            return resultado;
            
        
        } 
    }
}
