namespace Calculadora
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.btRaizCuadrada = new System.Windows.Forms.Button();
            this.btCambiarSigno = new System.Windows.Forms.Button();
            this.btSumAMem = new System.Windows.Forms.Button();
            this.btLeerMem = new System.Windows.Forms.Button();
            this.btBorrarMem = new System.Windows.Forms.Button();
            this.btTantoPorCiento = new System.Windows.Forms.Button();
            this.btIgual = new System.Windows.Forms.Button();
            this.btComaDec = new System.Windows.Forms.Button();
            this.btDigito0 = new System.Windows.Forms.Button();
            this.btMas = new System.Windows.Forms.Button();
            this.btPor = new System.Windows.Forms.Button();
            this.btDigito3 = new System.Windows.Forms.Button();
            this.btDigito2 = new System.Windows.Forms.Button();
            this.btDigito1 = new System.Windows.Forms.Button();
            this.btMenos = new System.Windows.Forms.Button();
            this.btDividir = new System.Windows.Forms.Button();
            this.btDigito6 = new System.Windows.Forms.Button();
            this.btDigito5 = new System.Windows.Forms.Button();
            this.btDigito4 = new System.Windows.Forms.Button();
            this.btIniciar = new System.Windows.Forms.Button();
            this.btBorrarEntrada = new System.Windows.Forms.Button();
            this.btDigito9 = new System.Windows.Forms.Button();
            this.btDigito8 = new System.Windows.Forms.Button();
            this.btDigito7 = new System.Windows.Forms.Button();
            this.etPantalla = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btRaizCuadrada
            // 
            this.btRaizCuadrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRaizCuadrada.Location = new System.Drawing.Point(153, 51);
            this.btRaizCuadrada.Name = "btRaizCuadrada";
            this.btRaizCuadrada.Size = new System.Drawing.Size(37, 30);
            this.btRaizCuadrada.TabIndex = 48;
            this.btRaizCuadrada.Text = "ra�z";
            this.btRaizCuadrada.Click += new System.EventHandler(this.btRaizCuadrada_Click);
            // 
            // btCambiarSigno
            // 
            this.btCambiarSigno.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCambiarSigno.Location = new System.Drawing.Point(196, 51);
            this.btCambiarSigno.Name = "btCambiarSigno";
            this.btCambiarSigno.Size = new System.Drawing.Size(37, 30);
            this.btCambiarSigno.TabIndex = 49;
            this.btCambiarSigno.Text = "+/-";
            this.btCambiarSigno.Click += new System.EventHandler(this.btCambiarSigno_Click);
            // 
            // btSumAMem
            // 
            this.btSumAMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSumAMem.Location = new System.Drawing.Point(97, 51);
            this.btSumAMem.Name = "btSumAMem";
            this.btSumAMem.Size = new System.Drawing.Size(37, 30);
            this.btSumAMem.TabIndex = 47;
            this.btSumAMem.Text = "M+";
            this.btSumAMem.Click += new System.EventHandler(this.btSumAMem_Click);
            // 
            // btLeerMem
            // 
            this.btLeerMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLeerMem.Location = new System.Drawing.Point(54, 51);
            this.btLeerMem.Name = "btLeerMem";
            this.btLeerMem.Size = new System.Drawing.Size(37, 30);
            this.btLeerMem.TabIndex = 46;
            this.btLeerMem.Text = "MR";
            this.btLeerMem.Click += new System.EventHandler(this.btLeerMem_Click);
            // 
            // btBorrarMem
            // 
            this.btBorrarMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBorrarMem.Location = new System.Drawing.Point(11, 51);
            this.btBorrarMem.Name = "btBorrarMem";
            this.btBorrarMem.Size = new System.Drawing.Size(37, 30);
            this.btBorrarMem.TabIndex = 45;
            this.btBorrarMem.Text = "MC";
            this.btBorrarMem.Click += new System.EventHandler(this.btBorrarMem_Click);
            // 
            // btTantoPorCiento
            // 
            this.btTantoPorCiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTantoPorCiento.Location = new System.Drawing.Point(196, 195);
            this.btTantoPorCiento.Name = "btTantoPorCiento";
            this.btTantoPorCiento.Size = new System.Drawing.Size(37, 30);
            this.btTantoPorCiento.TabIndex = 43;
            this.btTantoPorCiento.Text = "%";
            this.btTantoPorCiento.Click += new System.EventHandler(this.btTantoPorCiento_Click);
            // 
            // btIgual
            // 
            this.btIgual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btIgual.Location = new System.Drawing.Point(153, 195);
            this.btIgual.Name = "btIgual";
            this.btIgual.Size = new System.Drawing.Size(37, 30);
            this.btIgual.TabIndex = 42;
            this.btIgual.Text = "=";
            this.btIgual.Click += new System.EventHandler(this.btOperacion_Click);
            // 
            // btComaDec
            // 
            this.btComaDec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btComaDec.Location = new System.Drawing.Point(97, 195);
            this.btComaDec.Name = "btComaDec";
            this.btComaDec.Size = new System.Drawing.Size(37, 30);
            this.btComaDec.TabIndex = 35;
            this.btComaDec.Text = ",";
            this.btComaDec.Click += new System.EventHandler(this.btComaDec_Click);
            // 
            // btDigito0
            // 
            this.btDigito0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito0.Location = new System.Drawing.Point(11, 195);
            this.btDigito0.Name = "btDigito0";
            this.btDigito0.Size = new System.Drawing.Size(80, 30);
            this.btDigito0.TabIndex = 25;
            this.btDigito0.Text = "0";
            this.btDigito0.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btMas
            // 
            this.btMas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMas.Location = new System.Drawing.Point(196, 159);
            this.btMas.Name = "btMas";
            this.btMas.Size = new System.Drawing.Size(37, 30);
            this.btMas.TabIndex = 41;
            this.btMas.Text = "+";
            this.btMas.Click += new System.EventHandler(this.btOperacion_Click);
            // 
            // btPor
            // 
            this.btPor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPor.Location = new System.Drawing.Point(153, 159);
            this.btPor.Name = "btPor";
            this.btPor.Size = new System.Drawing.Size(37, 30);
            this.btPor.TabIndex = 40;
            this.btPor.Text = "x";
            this.btPor.Click += new System.EventHandler(this.btOperacion_Click);
            // 
            // btDigito3
            // 
            this.btDigito3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito3.Location = new System.Drawing.Point(97, 159);
            this.btDigito3.Name = "btDigito3";
            this.btDigito3.Size = new System.Drawing.Size(37, 30);
            this.btDigito3.TabIndex = 28;
            this.btDigito3.Text = "3";
            this.btDigito3.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito2
            // 
            this.btDigito2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito2.Location = new System.Drawing.Point(54, 159);
            this.btDigito2.Name = "btDigito2";
            this.btDigito2.Size = new System.Drawing.Size(37, 30);
            this.btDigito2.TabIndex = 27;
            this.btDigito2.Text = "2";
            this.btDigito2.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito1
            // 
            this.btDigito1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito1.Location = new System.Drawing.Point(11, 159);
            this.btDigito1.Name = "btDigito1";
            this.btDigito1.Size = new System.Drawing.Size(37, 30);
            this.btDigito1.TabIndex = 26;
            this.btDigito1.Text = "1";
            this.btDigito1.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btMenos
            // 
            this.btMenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btMenos.Location = new System.Drawing.Point(196, 123);
            this.btMenos.Name = "btMenos";
            this.btMenos.Size = new System.Drawing.Size(37, 30);
            this.btMenos.TabIndex = 39;
            this.btMenos.Text = "-";
            this.btMenos.Click += new System.EventHandler(this.btOperacion_Click);
            // 
            // btDividir
            // 
            this.btDividir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDividir.Location = new System.Drawing.Point(153, 123);
            this.btDividir.Name = "btDividir";
            this.btDividir.Size = new System.Drawing.Size(37, 30);
            this.btDividir.TabIndex = 38;
            this.btDividir.Text = "/";
            this.btDividir.Click += new System.EventHandler(this.btOperacion_Click);
            // 
            // btDigito6
            // 
            this.btDigito6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito6.Location = new System.Drawing.Point(97, 123);
            this.btDigito6.Name = "btDigito6";
            this.btDigito6.Size = new System.Drawing.Size(37, 30);
            this.btDigito6.TabIndex = 31;
            this.btDigito6.Text = "6";
            this.btDigito6.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito5
            // 
            this.btDigito5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito5.Location = new System.Drawing.Point(54, 123);
            this.btDigito5.Name = "btDigito5";
            this.btDigito5.Size = new System.Drawing.Size(37, 30);
            this.btDigito5.TabIndex = 30;
            this.btDigito5.Text = "5";
            this.btDigito5.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito4
            // 
            this.btDigito4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito4.Location = new System.Drawing.Point(11, 123);
            this.btDigito4.Name = "btDigito4";
            this.btDigito4.Size = new System.Drawing.Size(37, 30);
            this.btDigito4.TabIndex = 29;
            this.btDigito4.Text = "4";
            this.btDigito4.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btIniciar
            // 
            this.btIniciar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btIniciar.Location = new System.Drawing.Point(153, 87);
            this.btIniciar.Name = "btIniciar";
            this.btIniciar.Size = new System.Drawing.Size(37, 30);
            this.btIniciar.TabIndex = 36;
            this.btIniciar.Text = "C";
            this.btIniciar.Click += new System.EventHandler(this.btIniciar_Click);
            // 
            // btBorrarEntrada
            // 
            this.btBorrarEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBorrarEntrada.Location = new System.Drawing.Point(196, 87);
            this.btBorrarEntrada.Name = "btBorrarEntrada";
            this.btBorrarEntrada.Size = new System.Drawing.Size(37, 30);
            this.btBorrarEntrada.TabIndex = 37;
            this.btBorrarEntrada.Text = "CE";
            this.btBorrarEntrada.Click += new System.EventHandler(this.btBorrarEntrada_Click);
            // 
            // btDigito9
            // 
            this.btDigito9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito9.Location = new System.Drawing.Point(97, 87);
            this.btDigito9.Name = "btDigito9";
            this.btDigito9.Size = new System.Drawing.Size(37, 30);
            this.btDigito9.TabIndex = 34;
            this.btDigito9.Text = "9";
            this.btDigito9.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito8
            // 
            this.btDigito8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito8.Location = new System.Drawing.Point(54, 87);
            this.btDigito8.Name = "btDigito8";
            this.btDigito8.Size = new System.Drawing.Size(37, 30);
            this.btDigito8.TabIndex = 33;
            this.btDigito8.Text = "8";
            this.btDigito8.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // btDigito7
            // 
            this.btDigito7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDigito7.Location = new System.Drawing.Point(11, 87);
            this.btDigito7.Name = "btDigito7";
            this.btDigito7.Size = new System.Drawing.Size(37, 30);
            this.btDigito7.TabIndex = 32;
            this.btDigito7.Text = "7";
            this.btDigito7.Click += new System.EventHandler(this.btDigito_Click);
            // 
            // etPantalla
            // 
            this.etPantalla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.etPantalla.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.etPantalla.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etPantalla.Location = new System.Drawing.Point(12, 11);
            this.etPantalla.Name = "etPantalla";
            this.etPantalla.Size = new System.Drawing.Size(222, 27);
            this.etPantalla.TabIndex = 44;
            this.etPantalla.Text = "0,";
            this.etPantalla.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "operador";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 307);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "numOperandos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 337);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "operando1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 370);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "operando2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 402);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "memoria";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(170, 276);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 55;
            this.label6.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(170, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(170, 337);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 57;
            this.label8.Text = "label8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(170, 369);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 58;
            this.label9.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(170, 402);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 59;
            this.label10.Text = "label10";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(87, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 60;
            this.button1.Text = "Comprobar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 498);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btRaizCuadrada);
            this.Controls.Add(this.btCambiarSigno);
            this.Controls.Add(this.btSumAMem);
            this.Controls.Add(this.btLeerMem);
            this.Controls.Add(this.btBorrarMem);
            this.Controls.Add(this.btTantoPorCiento);
            this.Controls.Add(this.btIgual);
            this.Controls.Add(this.btComaDec);
            this.Controls.Add(this.btDigito0);
            this.Controls.Add(this.btMas);
            this.Controls.Add(this.btPor);
            this.Controls.Add(this.btDigito3);
            this.Controls.Add(this.btDigito2);
            this.Controls.Add(this.btDigito1);
            this.Controls.Add(this.btMenos);
            this.Controls.Add(this.btDividir);
            this.Controls.Add(this.btDigito6);
            this.Controls.Add(this.btDigito5);
            this.Controls.Add(this.btDigito4);
            this.Controls.Add(this.btIniciar);
            this.Controls.Add(this.btBorrarEntrada);
            this.Controls.Add(this.btDigito9);
            this.Controls.Add(this.btDigito8);
            this.Controls.Add(this.btDigito7);
            this.Controls.Add(this.etPantalla);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btRaizCuadrada;
    private System.Windows.Forms.Button btCambiarSigno;
    private System.Windows.Forms.Button btSumAMem;
    private System.Windows.Forms.Button btLeerMem;
    private System.Windows.Forms.Button btBorrarMem;
    private System.Windows.Forms.Button btTantoPorCiento;
    private System.Windows.Forms.Button btIgual;
    private System.Windows.Forms.Button btComaDec;
    private System.Windows.Forms.Button btDigito0;
    private System.Windows.Forms.Button btMas;
    private System.Windows.Forms.Button btPor;
    private System.Windows.Forms.Button btDigito3;
    private System.Windows.Forms.Button btDigito2;
    private System.Windows.Forms.Button btDigito1;
    private System.Windows.Forms.Button btMenos;
    private System.Windows.Forms.Button btDividir;
    private System.Windows.Forms.Button btDigito6;
    private System.Windows.Forms.Button btDigito5;
    private System.Windows.Forms.Button btDigito4;
    private System.Windows.Forms.Button btIniciar;
    private System.Windows.Forms.Button btBorrarEntrada;
    private System.Windows.Forms.Button btDigito9;
    private System.Windows.Forms.Button btDigito8;
    private System.Windows.Forms.Button btDigito7;
    private System.Windows.Forms.Label etPantalla;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}

