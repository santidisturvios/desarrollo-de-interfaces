using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Calculadora
{
  public partial class Form1 : Form
  {
    private enum Entrada //https://docs.microsoft.com/es-es/dotnet/csharp/language-reference/builtin-types/enum
        {
      NINGUNA,
      DIGITO,
      OPERADOR,
      CE,
      MEMMAS
    }

    private Entrada ultimaEntrada; //Ha de ser uno de los valores anteriores: ninguna, digito, operador, ce o memoria mas
    private bool comaDecimal;
    private char operador;
    private byte numOperandos;
    private double operando1;
    private double operando2;
    private double memoria;



    public Form1()
    {
      InitializeComponent();
      ultimaEntrada = Entrada.NINGUNA; //inicializo las variables
      comaDecimal = false;
      operador = '\0';
      numOperandos = 0;
      operando1 = 0;
      operando2 = 0;
      memoria = 0;
    }

    private void btDigito_Click(object sender, EventArgs e)
    {
      Button objButton = (Button)sender;

      if (ultimaEntrada != Entrada.DIGITO) //si entro un digito y no ha sido un digito lo introducido anterior
      {
        if (objButton.Text == "0") //si es 0 no hago nada
        {
          return;
        }
        etPantalla.Text = ""; //borro lo que haya en pantalla
        ultimaEntrada = Entrada.DIGITO; //entro el digito
        comaDecimal = false; //y le digo que no es una coma lo introducido
      }

      etPantalla.Text += objButton.Text; //si lo que hab�a entrado antes era un digito
    }

    private void btComaDec_Click(object sender, EventArgs e) //introduzco la coma
    {
      if (ultimaEntrada != Entrada.DIGITO) //si parte de que no se hab�an introducido datos
      {
        etPantalla.Text = "0,"; //escribo 0,
        ultimaEntrada = Entrada.DIGITO; //y el dato introducido
      }
      else if (comaDecimal == false) //si se hab�an introducido datos y no se habia introducizo coma
        etPantalla.Text = etPantalla.Text + ","; //escribo la coma

      comaDecimal = true; //variable de que se ha introducido coma ya
    }

    private void btOperacion_Click(object sender, EventArgs e)
    {
      // Obtener el id del bot�n que gener� el evento
      Button objButton = (Button)sender;
      // Obtener el texto del bot�n pulsado
      string textoBoton = objButton.Text;

      if ((numOperandos == 0) && (textoBoton[0] == '-')) //Caso particular no hay digitos y resto
        ultimaEntrada = Entrada.DIGITO;

      if (ultimaEntrada == Entrada.DIGITO)
        numOperandos += 1; //https://docs.microsoft.com/es-es/dotnet/csharp/language-reference/operators/arithmetic-operators

      if (numOperandos == 1) //Si solo he introducido un operando antes
        operando1 = double.Parse(etPantalla.Text);
      else if (numOperandos == 2)
      {
        operando2 = double.Parse(etPantalla.Text);
        switch (operador)
        {
          case '+':
            operando1 += operando2;
            break;
          case '-':
            operando1 -= operando2;
            break;
          case 'x':
            operando1 *= operando2;
            break;
          case '/':
            operando1 /= operando2;
            break;
          case '=':
            operando1 = operando2;
            break;
        }
        // Visualizar el resultado
        etPantalla.Text = operando1.ToString();
        numOperandos = 1;
      }
      operador = textoBoton[0]; // car�cter de la posici�n 0
      ultimaEntrada = Entrada.OPERADOR;
    }

    private void btTantoPorCiento_Click(object sender, EventArgs e)
    {
      double resultado;
      if (ultimaEntrada == Entrada.DIGITO)
      {
        resultado = operando1 * double.Parse(etPantalla.Text) / 100;
        // Visualizar el resultado
        etPantalla.Text = resultado.ToString();
        // Simular que se ha hecho clic en "="
        btIgual.PerformClick();
        // Enfocar la tecla %
        btTantoPorCiento.Focus();
      }
    }

    private void btIniciar_Click(object sender, EventArgs e)
    {
      etPantalla.Text = "0,";
      ultimaEntrada = Entrada.NINGUNA;
      comaDecimal = false;
      operador = '\0';
      numOperandos = 0;
      operando1 = 0;
      operando2 = 0;
    }

    private void btBorrarEntrada_Click(object sender, EventArgs e)
    {
      etPantalla.Text = "0,";
      ultimaEntrada = Entrada.CE;
      comaDecimal = false;

      
    }

    private void btCambiarSigno_Click(object sender, EventArgs e)
    {
      // Cambiar el signo del dato mostrado en la pantalla
      double resultado;
      resultado = -double.Parse(etPantalla.Text);
      // Visualizar el resultado
      etPantalla.Text = resultado.ToString();

      
        }

    private void btRaizCuadrada_Click(object sender, EventArgs e)
    {
      // Ra�z cuadrada
      double resultado;
      resultado = Math.Sqrt(double.Parse(etPantalla.Text));
      // Visualizar el resultado
      etPantalla.Text = resultado.ToString();

            
        }

    private void btSumAMem_Click(object sender, EventArgs e)
    {
      memoria += double.Parse(etPantalla.Text);
      ultimaEntrada = Entrada.MEMMAS;

            
        }

    private void btLeerMem_Click(object sender, EventArgs e)
    {
      // Leer el valor de memoria
      etPantalla.Text = memoria.ToString();

            
        }

    private void btBorrarMem_Click(object sender, EventArgs e)
    {
      // Borrar memoria
      memoria = 0;

           
        }

        private void asignarvariables() {

            label6.Text = "hola";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label6.Text = operador.ToString();
            label7.Text = numOperandos.ToString();
            label8.Text = operando1.ToString();
            label9.Text = operando2.ToString();
            label10.Text = memoria.ToString();
            


        }
    }
}