﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    class Program
    {
        static void Main(string[] args)
        {

            string miXML = @"..\DatosXML\archivo.xml"; //la @ evita problemas de compilación en cadenas
            
            DataSet ds = new DataSet(); //Creo la clase ds para acceder a datos




            try //evito posibles excepciones y si las hay las muestro
            {
                ds.ReadXml(miXML);
                
                Console.WriteLine(ds.GetXml());  //Nos muestra el archivo completo
                Console.WriteLine(ds.Tables.Count); //Nos muestra el numero de tablas
                Console.WriteLine(ds.Tables[0].Rows.Count); //Nos muesra el numero de filas en una columna
                Console.WriteLine(ds.Tables[0].Rows[1]["nombre"].ToString()); //Nos muestra una tupla en concreto
            
            
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            

            Console.ReadKey();
        }
    }
}
