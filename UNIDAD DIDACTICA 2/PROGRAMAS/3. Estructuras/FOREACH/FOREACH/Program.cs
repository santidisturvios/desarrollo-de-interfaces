﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOREACH
{
    class Program
    {
        static void Main(string[] args)
        {

            var ArrayNumeros = new List<int> { 0, 1, 1, 2, 3, 5, 8, 13 };
            int count = 0;
            foreach (int elementos in ArrayNumeros)
            {
                count++;
                Console.WriteLine($"Element #{count}: {elementos}");
            }
            Console.WriteLine($"Number of elements: {count}");

            Console.ReadKey();
        }
    }
}
