﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IF
{
    class Program
    {
        static void Main(string[] args)
        {
            float dividendo = 0.0f;
            float divisor = 1.0f;
            float resultado = 0.0f;
            string valor = "";

            Console.WriteLine("Dame el dividendo");
            valor = Console.ReadLine();
            dividendo = Convert.ToSingle(valor);

            Console.WriteLine("Dame el divisor");
            valor = Console.ReadLine();
            divisor = Convert.ToSingle(valor);

            if (divisor != 0.0f && divisor > 0.1f)
            {
                resultado = dividendo / divisor;
                Console.WriteLine("El resultado de la division es {0}", resultado);
            }

            else
            {
                Console.WriteLine("Division por 0");
            }




            

            Console.ReadKey();
         
        }
    }
}
