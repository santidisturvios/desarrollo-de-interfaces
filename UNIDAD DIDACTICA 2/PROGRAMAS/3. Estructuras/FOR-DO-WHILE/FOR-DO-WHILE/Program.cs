﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FOR_DO_WHILE
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            /*FOR*/

            for (n = 3; n <= 10; n++) {
                Console.WriteLine("{0}", n);
            }
            Console.ReadKey();

            for (n = 0; n <= 10; n+=2)
            {
                Console.WriteLine("{0}", n);
            }
            Console.ReadKey();

            /*DO*/

            do
            {
            
                n++;
                Console.WriteLine("{0}", n);
            }
            while (n < 20);

            Console.ReadKey();

            /*WHILE*/

            while (n < 30)
            {
                n++;
                Console.WriteLine("{0}", n);

            }
            Console.ReadKey();



        }



    }
}
