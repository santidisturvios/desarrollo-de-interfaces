﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedir_Datos_Mostrar_Datos
{
    class Program
    {
        static void Main(string[] args)
        {
            string entrada;
            string salida;
            int b;

            Console.WriteLine("Introduzca un dato");

            entrada = Console.ReadLine();
            Console.WriteLine("Su dato es {0}", entrada);

            //b = Convert.ToInt32(entrada);
            b = Int32.Parse(entrada);
            b = b * b;

            Console.WriteLine("El dato procesado es {0}, el dato antes de procesar {1}", b, entrada);

            

            

            Console.ReadKey();

        }
    }
}
