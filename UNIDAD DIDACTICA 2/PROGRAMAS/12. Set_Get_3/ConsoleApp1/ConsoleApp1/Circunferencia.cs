﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Circunferencia
    {
        public double Radio { get; set; }
        const double PI = 3.1415926;
        public Circunferencia(double radio){
            this.Radio = radio;
        }
        
        
        
        public double Perimetro{
            
            get{ return 2 * PI * this.Radio;}
        }
        public double Area{
            get{ return PI * Math.Pow(this.Radio, 2); }
        }

    }
}
