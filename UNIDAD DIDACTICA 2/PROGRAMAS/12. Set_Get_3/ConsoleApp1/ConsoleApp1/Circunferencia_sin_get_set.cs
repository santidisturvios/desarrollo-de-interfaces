﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Circunferencia_sin_get_set
    {
        public double radio;
        public double perimetro;
        public Circunferencia_sin_get_set(double radio) {
            this.radio = radio;          
        }
        public double calcularperimetro()
        {
            this.perimetro = this.radio * 3.14d;
            return this.perimetro;
        }

    }
}
