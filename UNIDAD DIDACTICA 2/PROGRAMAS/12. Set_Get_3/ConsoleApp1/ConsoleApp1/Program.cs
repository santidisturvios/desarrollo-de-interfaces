﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia circulo = new Circunferencia(25);
            Console.WriteLine(circulo.Area);
            circulo.Radio = 50; //cambio el radio de la circunferencia y se modifica el perimetro
            Console.WriteLine(circulo.Area);

            Circunferencia_sin_get_set circulo2 = new Circunferencia_sin_get_set(25);
            Console.WriteLine(circulo2.calcularperimetro());
            circulo2.radio = 50;
            Console.WriteLine(circulo2.perimetro); //He cambiado el radio y el perimetro no se modifica


            Console.ReadKey();
        }
    }
}
