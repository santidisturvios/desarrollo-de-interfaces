﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fecha
{
    class Date
    {
        private int mes;
        public int Mes {
            get { return mes; }
            set { if (value > 0 && value < 13)
                    mes = value;
            }
        }
    }
}
