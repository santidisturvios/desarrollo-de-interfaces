﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Sin_Get_Set a = new Sin_Get_Set();
            a.setNombre("Nombre Sin Get Set");
            Console.WriteLine(a.getNombre());


            Con_Get_Set b = new Con_Get_Set();
            b.Nombre = "Nombre Con Get Set";
            Console.WriteLine(b.Nombre); //acceso al metodo get

            Get_Set_simpli c = new Get_Set_simpli();
            c.simpli = "Nombre simplificado";
            Console.WriteLine(c.simpli);


            Persona persona = new Persona();
            persona.name = "Juan";
            persona.edad = 21;


            Console.WriteLine("Se llama {0} y tiene {1} años", persona.name, persona.edad );


            Console.ReadKey();


        }
    }
}
