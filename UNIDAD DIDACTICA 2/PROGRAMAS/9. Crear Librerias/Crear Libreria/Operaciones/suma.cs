﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operaciones
{
    public class suma
    {
        public static decimal Sumar(decimal a, decimal b) { //Estatico porque no necesita crear un objeto
            return a + b;
        }
    }
}
