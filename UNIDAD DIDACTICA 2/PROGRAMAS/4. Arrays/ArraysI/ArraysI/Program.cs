﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysI
{
    class Program
    {
        static void Main(string[] args)
        {

            // Array unidimensional de 5 integers
            int[] array1 = new int[5];

            // Lo mismo pero asignadole valores
            int[] array2 = new int[] { 1, 3, 5, 7, 9 };

            // Otra manera
            int[] array3 = { 1, 2, 3, 4, 5, 6 };

            // Array bidimensional
            int[,] multiDimensionalArray1 = new int[2, 3];

            // Bidimensional añadiendo valores
            int[,] multiDimensionalArray2 = { { 1, 2, 3 }, { 4, 5, 6 } };


            Console.WriteLine(array2[2]);

            Console.ReadKey();

            Console.WriteLine(multiDimensionalArray2[1,0]);

            Console.ReadKey();

            foreach (int i in array2)
            {
                Console.WriteLine(i);
            }

            Console.ReadKey();

        }
    }
}
