﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetosDeObjetos
{
    class Program
    {
        static void Main(string[] args)
        {
            //object Objeto;
            //Objeto new = ClaseBasica();

            ClaseBasica Objeto = new ClaseBasica();

            ClaseBasica a;
            a = (ClaseBasica)Objeto;

            Console.WriteLine(a.i);

            ClaseBasica b = new ClaseBasica();
            Console.WriteLine(b.i);

            Console.ReadKey();
        }
    }
}
