﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Base
    {
        public string Nombre { set; get; }
        public string Apellido = "defecto";

        public virtual void IntApellido(string apellido) { //permitimos que se pueda cambiar en herencias

            this.Apellido = apellido;
        }
    }
}
