﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Total alumnos antes:" + Alumno.Contador.ToString()); //llamo al método estático
            

            //Creo los alumnos y accedo a las propiedades de su clase base
            Alumno a = new Alumno("Maria", 39);
            ((Padrino)a).NombreP = "Luciana";
            Alumno b = new Alumno("Pedro", 33);
            ((Padrino)b).NombreP = "Ramón";


            Console.WriteLine("Total alumnos después:" + Alumno.Contador.ToString());
            a.Imprime();
            Console.WriteLine("Padrino: {0}", ((Padrino)a).NombreP);
            b.Imprime();
            Console.WriteLine("Padrino: {0}", ((Padrino)b).NombreP);
            Console.ReadLine();
        }
    }
}
