﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuncionesYMetodosI
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = 5;
            int b = 10;
            int suma = f_sumador(a, b); //llamada funcion
            Console.WriteLine("(Funcion) La suma es " + suma);
            m_sumador(a, b); //Llamada metodo

            Console.ReadKey();

        }
        //Declaracion Funcion
        public static int f_sumador(int a, int b)
        {
            int suma = a + b;
            return suma;  //La función ha diferencia de un método devuelve un valor
        }

        //Declaracion Metodo
        public static void m_sumador(int a, int b)
        {
            Console.WriteLine("(Metodo) La suma es " + (a + b));
        }
    }
}
