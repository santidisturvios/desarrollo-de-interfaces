﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace Set_Get_Cliente_Banco
{
    class Cliente
    {
        public double TotalCompra{ get;  set; }
        public string Name { get; set; }
        public int ClienteID { get; set; }
        public int interes { get; }

        //Construimos el cliente
        public Cliente(double compras, string name, int id) {
            TotalCompra = compras;
            Name = name;
            ClienteID = id;
            interes = 10;
        
        }

    }
}
