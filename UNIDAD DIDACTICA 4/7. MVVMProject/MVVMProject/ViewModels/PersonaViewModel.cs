﻿using MVVMProject.Connectors;
using MVVMProject.Core.Commands;
using MVVMProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MVVMProject.ViewModels
{
    public class PersonaViewModel : IGeneric
    {
        private PersonaCollection _listaPersonas = new PersonaCollection();
        public PersonaCollection ListaPersonas
        {
            get { return _listaPersonas; }
            set
            { 
                _listaPersonas = value;
                if(value != null && value.Count > 0)
                {
                    CurrentPersona = value[0];
                }
                RaisePropertyChanged("ListaPersonas");                
            }
        }

        private Persona _currentPersona;
        public Persona CurrentPersona
        {
            get { return _currentPersona; }
            set 
            { 
                _currentPersona = value;
                RaisePropertyChanged("CurrentPersona");
                RaisePropertyChanged("CanShowInfo");
            }
        }

        private ICommand _listarPersonasCommand;
        public ICommand ListarPersonasCommand
        {
            get
            {
                if (_listarPersonasCommand == null)
                    _listarPersonasCommand = new RelayCommand(new Action(ListarPersonas));
                return _listarPersonasCommand;
            }
        }

        private ICommand _verInfoCommand;
        public ICommand VerInfoCommand
        {
            get
            {
                if (_verInfoCommand == null)
                    _verInfoCommand = new RelayCommand(new Action(VerInfo), () => CanShowInfo);
                return _verInfoCommand;
            }
        }
        private ICommand _verInfo2Command;
        public ICommand VerInfo2Command
        {
            get
            {
                if (_verInfo2Command == null)
                    _verInfo2Command = new ParamCommand(new Action<object>(VerInfo2));
                return _verInfo2Command;
            }
        }

        private ICommand _eliminarPersonaCommand;
        public ICommand EliminarPersonaCommand
        {
            get
            {
                if (_eliminarPersonaCommand == null)
                    _eliminarPersonaCommand = new ParamCommand(new Action<object>(EliminarPersona));
                return _eliminarPersonaCommand;
            }
        }

        private bool CanShowInfo
        {
            get
            {
                return CurrentPersona != null;
            }
        }

        public PersonaViewModel()
        {

        }

        private void ListarPersonas()
        {
            ListaPersonas = App.DbConnector.listarPersonas();
        }

        private void VerInfo2(object obj)
        {
            if(obj != null)
            {
                CurrentPersona = (Persona)obj;
                MessageBox.Show(CurrentPersona.Nombre);
            }           
        }

        private void EliminarPersona(object obj)
        {
            if (obj != null)
            {
                CurrentPersona = (Persona)obj;
                if(App.DbConnector.eliminarPersona(CurrentPersona))
                {
                    if (MessageBox.Show("Eliminado " + CurrentPersona.Nombre + "!") == MessageBoxResult.OK)
                    {
                        ListaPersonas.Remove(((Persona)obj));
                    }
                } 
            }    
        }

        private void VerInfo()
        {
            MessageBox.Show(CurrentPersona.Nombre);
        }
    }
}
