﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MVVM_Intro
{
    class AnimalViewModel :INotifyPropertyChanged
    {
        private Animal _model;

        public AnimalViewModel()
        {
            _model = new Animal { Name = "Cat", Gender = "Male" };
        }

        public string AnimalName
        {
            get { return _model.Name; }
            set
            {
                _model.Name = value;
                OnPropertyChanged("AnimalName");
            }
        }

        public string AnimalGender
        {
            get { return _model.Gender; }
            set
            {
                _model.Gender = value;
                OnPropertyChanged("AnimalGender");
            }
        }

        //Event binds view to ViewModel.
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                this.PropertyChanged(this, e);
            }
        }

    }
}
